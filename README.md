[![Licencia de Creative Commons](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/) Este obra está bajo una [licencia de Creative Commons Reconocimiento-NoComercial-SinObraDerivada 4.0 Internacional](http://creativecommons.org/licenses/by-nc-nd/4.0/).
# Proyecto Laberinto UF2
Bienvenido a la documentación oficial del proyecto UF2 laberinto. En este documento explicaremos cómo funciona este código.

Pagina de JavaDoc: https://repomatt.gitlab.io/proyecto-2-laberinto/
# Esquema principal
### Menu
Cuando inicias el juego, te saldra un menu con varios opciones. La primera opcion es para jugar en un laberinto. Dentro de la primera opcion (**Jugar Partida**)
Te saldra un menu para que eligas que dificultad quieres jugar. La lista de dificultades son: Facil, Medio, Dificl, Creadas,Generado


En la segunda opcion tenemos la posibilidad de mirar nuestros resultados que hemos hecho. Dentro de **Resultados de Partida** nos
saldra un menu para poder ver nuestros logros segun por dificultad o todo. La lista de dificultades que se puedan ver son: Facil, Medio, Dificl, Creadas, Generados,*Todos*


En la tercera opcion tenemos la posibilidad de crear nuestra propia tabla, y jugarla. Dentro de **Crear Tabla** nos saldra un menu que podemos elegir
o el tutorial donde explica con detalle como crear tu propio laberinto y luego Crear Laberinto donde creamos el laberinto.


Y por ultimo la ultima opcion es para salir del programa.
# Planning
En esta fase, comenzamos a pensar cómo podríamos crear el movimiento y también controlar que no golpee una pared. Decidimos utilizar la instrucción switch y no IF ELSE porque usar IF ELSE haría que nuestro código fuera largo.  
![Switch](imatges/switch.JPG)
  
Una vez hemos planeado el sistema de movimiento, comenzamos a pensar cómo podríamos hacer el laberinto. Al principio no sabíamos qué caracter sería el muro,
qué caracter sería el jugador y qué caracter sería el final. Así que acordamos que # serían las paredes,  @ sería el jugador y ! seria el fin.  
![Laberinto fácil](imatges/easy.JPG)
  
En este punto, hemos planeado cómo vamos a hacer el movimiento, cómo se crearán los laberintos, ahora tuvimos que pensar cómo vamos a juntar todos los niveles y convertir 
esta idea en un juego.Terminamos haciendo una funcion que sera responsable de una parte del programa. Como por ejemplo, una funcion que guarde los datos,
una function que se encarga de los movientos, una funcion que nos elige el laberinto y mucho mas.  
![IF dentro de game](imatges/if_game.JPG)

Volviendo a los laberintos, pusimos dos ejemplos de laberintos por nivel. Los de nivel Fàcil los generamos mediante bucles for, primero inicializamos toda la matriz con #
y después con otro bucle for agregamos espacios en blanco al laberinto y por último ponemos la posición del jugador y la posición final.  
![laberinto_facil](imatges/lbt-facil.JPG)

Los de nivel medio i difícil los generamos directamente escribiendo los valores de cada posición del array manualmente diseñandolos previamente sobre papel.  
![laberinto_medio](imatges/lbt-medio.JPG)  
También tuvimos que pensar que variables necesitariamos tener constantes para poder utilizarlas en varias funciones.
# Distribución de las funciones
- **seleccionar**: Función que muestra el menú inicial.  
- **mostrarLaberint**: Funció para cargar el laberinto que el jugador ha seleccionado.  
- **moverLaberint**: Función encargada de mover al jugador por el laberinto.  
- **printarLaberint**: Función que muestra en la consola la posición en la que está el jugador después de realizar un movimiento.  
- **mostrarDatos**: Muestra el menú para cargar los datos guardados.  
- **ensenarDatos**: Lee los datos del .txt y los muestra por consola.  
- **guardarInfo**: Guarda los datos de las partidas en un .txt (nivel, movimientos...).  
- **crearTabla**: Menú con el tutorial y la opcion de permitir al jugador jugar en sus propios laberintos.  
- **cargarTabla**: Función que permite al jugador cargar sus propios laberintos para jugarlos después.  

# Propuestas de mejora  
- Dividir el programa en más funciones.  
- Controlar la carga del array "laberinth" mediante clases, al final no supimos implementarlo correctamente.  

  
