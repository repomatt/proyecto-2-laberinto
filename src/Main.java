import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
/**
  @author  Matthew Calcagno
  @author Raul Delgado
  @version 1.1.0
  @since 3/5/2019
 */

/**
 * Main class del juego
 */
public class Main {
  /**
   * Fila posicion para ganar
   */
  public static int winRow;
  /**
   * Columna posicion para ganar
   */
  public static int winColumn;
  /**
   * Posicion actual del jugador (desde la fila)
   */
  public static int playerRow;
  /**
   * Posicion actual del jugador (desde la columna)
   */
  public static int playerColumn;
  /**
   * Numero de filas del laberinto
   */
  public static int files;
  /**
   * Numero de columnas en el laberinto
   */
  public static int columnes;
  /**
   * La dificultad
   */
  public static String nivellT = "";
  /**
   * Numero de nivel representado por un numero
   */
  public static int nivell;
  /**
   * Es el contador que cuenta los pasos que hace el jugador
   */
  public static int counter;
  /**
   * Si el jugador pulse la P para retirar de la partida.
   */
  public static boolean retire;
  /**
   * El laberinto donde el jugador juega
   */
  public static String[][] laberinth;

  /**
   * Funcion principal del juego
   * @param args
   * @throws IOException
   * @throws InterruptedException
   */

  public static void main(String[] args) throws IOException, InterruptedException {

    seleccionar();

  }

  //funcion
  /**
   *
   * Esta funcion se encarga de que el usuario puede elegir que quiere hacer, tanto como jugar partidas, ver resultas y mucho mas
   */
  public static void seleccionar () throws IOException, InterruptedException {

    /**
     * @param opcionAux es el String donde se comprobara que el valor sea un numero, en caso que es cierto pasa a opcion
     * @param opcion elegido por el usuario
     */
    Scanner scan = new Scanner(System.in);
    // Menu
    int opcion = 0;
    String opcionAux;
    System.out.println("=============================================");
    System.out.println("1. Jugar Partida");
    System.out.println("2. Resultados de Partida");
    System.out.println("3. Crear Tabla");
    System.out.println("4. Salir");
    System.out.println("=============================================");

    System.out.println("Porfavor seleccione una opcion (numeros) ");
    // Primero preguntamos por el valor que sera un nextLine
    opcionAux = scan.nextLine();
    if (Character.isDigit(opcionAux.charAt(0))){
      //Si el valor es un numero, entonces opcion sera igual a opcionAux, en case que no es un numero, te preguntara otra vez
      opcion=Integer.parseInt(opcionAux);
    }
    if(opcion == 4){
      //En caso que el usuario introduzca la opcion 4, el programa se acabara
        System.exit(0);
    } else if (opcion == 2){
        mostrarDatos();
    } else if (opcion == 1) {
        System.out.println("1. Facil");
        System.out.println("2. Medio");
        System.out.println("3. Dificil");
        System.out.println("4. Generar Laberinto");

      opcionAux = scan.nextLine();
      if (Character.isDigit(opcionAux.charAt(0))){
        nivell=Integer.parseInt(opcionAux);
      }

      if (nivell == 1) {
        //El tema de niveles funciona de la siguiente manera:
        // El usuario introduze un numero entre 1 a 3 (solo se aplica entre facil, medio y dificil)
        // Despues que el usuario juege el primer laberinto se le sumarara 4 y se redirige al siguiente laberinto
        //Por ejemplo, si el usuario introduze 1, primero juega el laberinto faciil (numero 1) y una vez completado
        //  Se le sumara 4 ya que es un numero que el usuario no podra introducir y se ira al laberinto nivel 5
        // Una vez completado el juego guarda los moviemientos que has hecho en total.
          printarLaberint(mostrarLaberint(nivell));
          nivell=nivell+4;

          System.out.println("Ultimo piso!");
          Thread.sleep(1000);

          printarLaberint(mostrarLaberint(nivell));
      } else if (nivell == 2) {

          printarLaberint(mostrarLaberint(nivell));
          nivell=nivell+4;

          System.out.println("Ultimo piso!");
          Thread.sleep(1000);
          printarLaberint(mostrarLaberint(nivell));
      } else if (nivell == 3) {
          printarLaberint(mostrarLaberint(nivell));
          nivell=nivell+4;
          System.out.println("Ultimo piso!");
          Thread.sleep(1000);

          printarLaberint(mostrarLaberint(nivell));
      } else if (nivell == 4) {
          printarLaberint(mostrarLaberint(nivell));
      } else {
        System.out.println("Nivel de dificultad erroneo!!!");
        System.out.println("Volviendo al menu de seleccion de nivel");
        Thread.sleep(2000);
        System.out.println("");
        seleccionar();
      }
    } else if (opcion == 3 ){
      crearTabla();
    } else {
      seleccionar();
    }
  }

  /**
   * Esta funcion muestra el laberinto que el jugador has elegido desde seleccionarNivell
   * @param n Es el numero que es pasado por seleccionarNivell donde se obtendra el laberinto correcto
   */
  public static String[][] mostrarLaberint (int n) throws IOException, InterruptedException {
    /**
     * Aqui lo unico que hace es que nos pondra el laberinto correcto dependiendo del nivel en el que el usuario este.
     * Si el usuario esta en el nivel 1 entonces esta funcion nos pondra el laberinto de nivel 1, etc.
     * En el caso de 1 y 5, no son laberintos hechos como el resto. Estan hechos con un par de  for's
     * Esta funcion tambien se encargara de dar el valor correcto de filas, columnas, la fila de victoria, la columna de victoria, etc.
     *
     */
    Scanner scan = new Scanner(System.in);
    if (n == 1){
      files=5;
      columnes=5;
      laberinth = new String[files][columnes];
      // Ponemos todo a #
      for (int i = 0; i < files; i++) {
        for (int j = 0; j < columnes; j++) {
          laberinth[i][j] = "#";
        }
      }
      // Aqui vamos agregando los espacios para que se pueda completar
      for (int j = 1; j < 4; j++) {
        laberinth[1][j] = " ";
      }
      // Posicion comienza el jugador
      laberinth[0][3] = "@";
      laberinth[2][2] = " ";
      // Posicion donde se acaba
      laberinth[3][2] = "!";

      winRow = 3;
      winColumn = 2;
      playerRow = 0;
      playerColumn = 3;
      nivellT = "Fácil";

      return laberinth;
    }
    if (n==5){
      files = 7;
      columnes = 7;
      laberinth = new String[files][columnes];
      for (int i = 0; i < files; i++) {
        for (int j = 0; j < columnes; j++) {
          laberinth[i][j] = "#";
        }
      }
      for (int j = 1; j < 4; j++) {
        laberinth[1][j] = " ";
      }
      laberinth[0][3] = "@";
      laberinth[2][2] = " ";
      laberinth[4][1] = " ";
      for (int j = 1; j < 3; j++) {
        laberinth[5][j] = " ";
      }

      for (int j = 1; j < 5; j++) {
        laberinth[3][j] = " ";
      }

      laberinth[5][2] = "!";
      winRow = 5;
      winColumn = 2;
      playerRow = 0;
      playerColumn = 3;
      nivellT = "Facil";

      return laberinth;
    }
    if (n == 2){
      laberinth = new String[][]{{"#","!","#","#","#","#","#","#","#","#","#","#","#","#"},
              {"#"," ","#"," "," "," "," "," "," "," "," "," "," ","#"},
              {"#"," ","#","#"," ","#","#","#","#","#","#","#"," ","#"},
              {"#"," ","#"," "," ","#"," "," "," "," "," ","#"," ","#"},
              {"#"," ","#","#"," ","#","#","#","#","#"," ","#"," ","#"},
              {"#"," "," "," "," "," "," "," "," ","#"," ","#"," ","#"},
              {"#"," ","#","#","#","#","#","#"," ","#"," ","#"," ","#"},
              {"#"," ","#"," "," "," "," ","#"," ","#"," ","#"," ","#"},
              {"#"," ","#","#"," ","#"," ","#"," "," "," ","#"," ","#"},
              {"#"," ","#","#"," ","#"," ","#","#","#"," ","#"," ","#"},
              {"#"," ","#"," "," ","#"," "," "," "," "," ","#"," ","#"},
              {"#"," ","#","#"," ","#","#","#","#","#","#","#"," ","#"},
              {"#"," "," "," "," ","#"," "," "," "," "," "," "," ","#"},
              {"#","#","#","#","@","#","#","#","#","#","#","#","#","#"},
      };
      files = 14;
      columnes = 14;
      winRow = 0;
      winColumn = 1;
      playerRow = 13;
      playerColumn = 4;
      nivellT = "Medio";
      return laberinth;
    } else if (n == 6){
      laberinth = new String[][]{{"#","#","#","#","#","#","#","#","#","#","#","#","#","#"},
              {"#"," "," "," "," "," "," "," ","#","#"," ","#","#","#"},
              {"#","#"," ","#","#","#","#"," ","#"," "," "," "," ","!"},
              {"#","#"," ","#","#"," ","#"," ","#"," ","#"," ","#","#"},
              {"#","#"," ","#"," "," ","#"," ","#"," ","#"," ","#","#"},
              {"#","#"," ","#","#"," ","#"," ","#"," ","#"," ","#","#"},
              {"#","#"," "," "," "," ","#"," ","#"," "," "," ","#","#"},
              {"#","#","#","#","#"," ","#"," ","#","#","#"," ","#","#"},
              {"#"," ","#"," ","#"," ","#"," "," "," "," "," ","#","#"},
              {"@"," ","#"," ","#"," ","#","#","#","#","#"," ","#","#"},
              {"#"," ","#"," ","#"," "," "," "," "," "," "," "," ","#"},
              {"#"," ","#"," ","#","#","#","#","#","#","#","#"," ","#"},
              {"#"," "," "," "," "," "," "," "," "," "," "," "," ","#"},
              {"#","#","#","#","#","#","#","#","#","#","#","#","#","#"},
      };
      files = 14;
      columnes = 14;
      winRow = 2;
      winColumn = 13;
      playerRow = 9;
      playerColumn = 0;
      nivellT = "Medio";
      return laberinth;
    }else if (n == 3){
      laberinth = new String[][]{{"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"},
              {"#","#","#","#"," ","#","#"," ","#","#"," ","#","#"," ","#","#","#","#","#","#","#","#"," "," "," "," "," "," "," ","#"," ","#","#"," ","#","#"," ","#"," ","#"," "," "," "," "," "," "," "," "," ","#"},
              {"#","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#","#","#","#","#"," "," "," "," "," "," "," "," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"},
              {"#","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," "," "," "," "," ","#","#","#","#","#","#","#","#","#"," ","#"," ","#"," ","#","#","#","#","#","#","#"," ","#"},
              {"#","#"," ","#"," "," ","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#"," ","#"," ","#"," "," "," "," "," "," "," ","#"," ","#"," ","#"," ","#"," "," "," "," "," "," "," ","#"},
              {"#","#"," ","#"," ","#","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," ","#"," ","#","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," ","#","#","#","#","#","#","#"},
              {"#","#"," ","#"," "," ","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#"," ","#"," ","#"," "," "," ","#"," ","#"," ","#"," ","#"," ","#"," "," "," "," "," "," "," ","#"},
              {"#","#"," ","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#","#","#","#","#","#","#"," ","#"},
              {"#","#"," ","#"," "," "," ","#"," ","#"," "," "," "," ","#"," "," "," "," ","#"," "," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#","#","#"," ","#"," ","#"," "," "," "," "," ","#"," ","#"},
              {"#","#"," ","#","#","#"," ","#"," "," "," ","#","#"," ","#"," ","#","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," "," "," "," ","#"," ","#"," ","#","#","#","#","#"," ","#"},
              {"#","#"," "," "," ","#"," ","#","#","#"," "," ","#"," ","#"," ","#"," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#","#","#","#","#","#","#"," ","#"," ","#"," "," "," "," "," ","#"},
              {"#","#"," ","#"," ","#"," ","#"," "," "," ","#","#"," ","#"," ","#","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," "," "," "," "," "," "," "," ","#"," ","#"," ","#"," ","#","#","#"},
              {"#","#","#","#"," ","#"," ","#"," ","#","#","#"," "," ","#"," "," ","#"," ","#"," ","#"," "," "," ","#"," "," "," ","#"," ","#","#","#","#","#","#","#","#","#","#","#"," ","#"," ","#","#","#"," ","#"},
              {"#","#"," "," "," ","#"," ","#"," "," "," ","#"," ","#","#"," ","#","#"," ","#"," ","#","#","#","#","#","#","#","#","#"," ","#","#"," "," "," ","#"," "," "," "," "," "," ","#"," "," "," "," "," ","#"},
              {"#","#"," ","#","#","#"," ","#","#","#"," ","#"," "," ","#"," "," ","#"," ","#"," "," "," "," "," "," "," "," "," "," "," ","#"," "," ","#"," ","#"," ","#"," ","#","#"," ","#","#","#","#","#"," ","#"},
              {"#","#"," "," "," ","#"," ","#"," "," "," ","#"," ","#","#","#"," ","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," ","#"," ","#"," ","#"," "," ","#"," "," "," ","#"," "," "," ","#"},
              {"#","#","#","#"," ","#"," ","#"," ","#","#","#"," ","#"," ","#"," ","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#"," ","#"," ","#"," ","#"," ","#","#","#"," ","#","#"," ","#","#","#"},
              {"@"," "," "," "," "," "," ","#"," ","#"," ","#"," "," "," ","#"," ","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," ","#","#","#","#"," "," "," ","#"},
              {"#","#"," ","#"," ","#"," "," "," ","#"," ","#"," ","#"," "," "," ","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#"," "," "," ","#"," "," "," "," "," "," ","#","#","#"," ","#"},
              {"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","!","#"}
      };
      files = 20;
      columnes = 50;
      winRow = 19;
      winColumn = 48;
      playerRow = 17;
      playerColumn = 0;
      nivellT = "Díficil";
      return laberinth;
    }
    else if (n == 4 ){
      //En el caso de 4, que es un laberinto generado funciona de la siguiente manera:
      // Primero hacemos un random entre 3 a 10, creara un laberinto de RANDOM x RANDOM y luego pone todo la tabla con una pared

      int randomNumber = (int )(Math.random() * 10 + 3);
      laberinth = new String[randomNumber][randomNumber];
      int rando = 0;
      for (int i = 0; i < laberinth.length; i++) {
        for (int j = 0; j < laberinth.length; j++) {
          laberinth[i][j] = "#";
        }
      }
      //Despues el programa mismo se asigna espacios en blanco para que el jugador pueda moverse
      for (int u = 0; u< randomNumber; u++) {
        for (int i = 0; i < laberinth.length; i++) {
          rando = (int) (Math.random() * randomNumber);
          laberinth[i][rando] = " ";
        }
      }

      //Una vez tengamos los espacios en blanco y los muros, generamos 2 numeros aleatorios y los asignamos a ! que es el simbolo de victoria
      int random1 = (int) (Math.random() * (randomNumber-1));
      int random2 = (int) (Math.random() * (randomNumber-1));
      //El punto de comienzo siempre sera la misma.
      laberinth[laberinth.length-1][0] = "@";
      while (laberinth[random1][random2].equals("@")) {
        random1 = (int) (Math.random() * (randomNumber-1));
        random2 = (int) (Math.random() * (randomNumber-1));
      }
      laberinth[random1][random2] = "!";
      winRow = random1;
      winColumn = random2;
      playerRow = laberinth.length-1;
      playerColumn = 0;
      nivellT = "Generado";
      files = randomNumber;
      columnes = randomNumber;

      return laberinth;
    }
    else if (n == 7){

      laberinth = new String[][]{{"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"},
                              {"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," "," ","#"," "," "," "," ","#","#","#","#","#","#","#","#","#","#","#","#","#","#"," "," "," ","#"," "," "," "," "," "," ","#"},
                              {"#","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#","#"," ","#"," ","#","#"," ","#"," "," "," "," "," ","#"," "," "," "," "," "," "," "," ","#"," ","#"," ","#","#","#","#","#","#"},
                              {"#","#","#","#","#","#"," ","#","#"," ","#","#","#","#","#","#"," ","#","#"," ","#"," ","#","#"," ","#","#","#"," ","#"," ","#"," ","#","#","#","#","#","#","#","#"," ","#"," ","#"," "," "," "," ","!"},
                              {"#"," "," "," "," "," "," ","#","#","#","#","#"," ","#","#","#"," ","#"," "," "," "," ","#"," "," "," ","#"," "," ","#"," ","#"," ","#"," "," "," "," ","#","#","#"," "," "," ","#"," ","#","#","#","#"},
                              {"#"," ","#","#","#","#","#","#","#"," "," "," "," "," "," ","#"," ","#"," ","#","#","#","#","#"," ","#","#","#"," ","#"," ","#"," ","#"," ","#","#"," ","#"," ","#","#","#","#","#"," "," "," ","#","#"},
                              {"#"," ","#"," ","#","#","#","#","#"," ","#","#","#","#"," ","#"," ","#"," ","#"," "," "," ","#"," ","#"," "," "," ","#"," "," "," ","#"," ","#","#"," ","#"," ","#","#"," "," "," "," "," "," ","#","#"},
                              {"#"," ","#"," ","#"," "," "," "," "," ","#"," "," ","#"," ","#"," ","#"," ","#"," ","#","#","#"," ","#","#"," ","#","#","#","#","#","#"," ","#","#"," ","#"," "," "," "," ","#","#","#"," "," ","#","#"},
                              {"#"," ","#"," ","#"," ","#"," ","#","#","#"," ","#","#"," ","#"," ","#"," ","#"," ","#"," "," "," "," ","#"," ","#"," "," "," "," "," "," ","#","#"," ","#"," ","#","#","#","#"," ","#","#","#","#","#"},
                              {"#"," ","#"," ","#"," ","#"," "," "," "," "," ","#","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#","#"," ","#"," ","#","#","#","#"," ","#","#"," ","#"," "," "," "," "," "," "," "," "," "," ","#"},
                              {"#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," ","#"," "," "," "," ","#","#","#","#"," ","#","#","#","#","#","#","#","#","#","#"," ","#"},
                              {"@"," "," "," "," "," ","#"," ","#"," ","#","#","#","#"," ","#"," ","#"," "," "," ","#"," ","#"," ","#","#"," ","#","#","#","#"," ","#"," "," "," "," "," "," ","#"," "," "," ","#"," "," ","#"," ","#"},
                              {"#","#","#","#","#","#","#"," ","#","#"," "," "," "," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," "," "," "," ","#"," ","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," ","#","#"," ","#"},
                              {"#"," "," "," "," "," "," "," ","#"," "," ","#","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," "," "," ","#","#","#"," ","#"," "," "," "," ","#"," ","#"," ","#"," ","#"," ","#"," ","#"," "," ","#"},
                              {"#"," ","#","#","#","#","#","#","#","#"," ","#"," "," "," "," "," ","#","#","#"," ","#"," ","#","#","#","#","#","#","#","#","#"," ","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," ","#","#"," ","#"},
                              {"#"," "," "," "," "," "," "," "," ","#"," ","#"," ","#","#","#","#","#"," ","#"," ","#"," "," "," "," "," "," "," "," "," "," "," ","#","#","#","#"," ","#"," ","#"," ","#"," ","#"," "," ","#"," ","#"},
                              {"#","#","#","#","#","#","#"," ","#","#"," ","#"," "," "," "," "," "," "," ","#"," ","#","#","#","#","#","#","#","#","#","#","#","#","#"," "," "," "," ","#"," "," "," ","#"," "," "," ","#","#"," ","#"},
                              {"#"," ","#"," "," "," ","#"," ","#"," "," ","#","#","#","#","#","#","#","#","#"," ","#"," "," "," "," "," "," "," "," "," "," "," "," "," ","#","#","#","#","#","#","#","#","#","#","#","#","#"," ","#"},
                              {"#"," "," "," ","#"," "," "," ","#","#"," "," "," "," "," "," "," "," "," "," "," ","#","#","#","#","#","#","#","#","#","#","#","#","#"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","#"},
                              {"#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"}
      };

      files = 20;
      columnes = 50;
      winRow = 3;
      winColumn = 49;
      playerRow = 11;
      playerColumn = 0;
      nivellT = "Díficil";
      return laberinth;
    }
    else {
      return laberinth;
    }
  }
  /**
   * Esta funciona va conjunto a mostrarLaberint donde una vez el laberinto es elegido, entonces la mecanica del movimiento
   * se pone en marcha.
   * @param laberint es el String bidimensional del laberinto
   *
   */
  public static void printarLaberint(String[][]laberint) throws IOException, InterruptedException {

    Scanner in = new Scanner(System.in);
    // contador para contar los pasos
    counter = 0;
    boolean win = false;
    retire = false;
    while (!win && !retire) {
// Para imprimir cada vez y verla actualizada
      for (int i = 0; i < files; i++) {
        for (int j = 0; j < columnes; j++) {
          System.out.print(laberint[i][j]);
        }
        System.out.println();
      }

      System.out.println("Pulse W A S D para mover");
      String movement = in.nextLine().toUpperCase();
      moverLaberint(movement);


      //Si el usuario se retira de la partida (pulsando la P)
      if (retire){
        //se saldra del bucle con cambiando win a true
        win = true;
        // Si el nivel es mas grande que 3 (primer laberinto de facil es 1, primer labeinrto de medio es 2 y primer labeinto de hard es 3)
        // Entonces se printara que ha salido del laberinto y volvera al menu sin guardar.
        if (nivell > 3) {
          System.out.println("Has salido del laberinto");
          seleccionar();
        }
      }
      if (playerRow == winRow && winColumn == playerColumn){
        //Si el jugador ha ganado entonces saldra el bucle con cambiando win a true
        win = true;
        //Misma condicion pero esta vez llama la guncion guardarInto y se pasa el contador y el nivel.
        if (nivell > 3) {
          guardarInfo(counter, nivellT);
        }
      }

    }
  }

  /**
   * Funcion principal del movimiento. Se encarga de mirar si el movimiento es valido en sentido que no haya un muro etc.
   * @param movement El valor pasado donde se valorara si el movimiento es valido o no
   */
  public static void moverLaberint (String movement){
    switch (movement) {

      case "W":
        if ((playerRow - 1 >= 0)) {
          // Como todo los casos, si donde quremos mover NO TIENE un # entonces nos dejara, si no, no hara nada.
          if (!(laberinth[playerRow - 1][playerColumn].equals("#"))) {
            // sumamos 1 al contador
            counter++;
            // borramos el @ antiguo
            laberinth[playerRow][playerColumn] = " ";
            playerRow--;
            // actualizamos el @
            laberinth[playerRow][playerColumn] = "@";
          }
        }
        break;
      case "S":
        if (playerRow + 1 < laberinth.length) {
          if (!(laberinth[playerRow + 1][playerColumn].equals("#"))) {
            counter++;
            laberinth[playerRow][playerColumn] = " ";
            playerRow++;
            laberinth[playerRow][playerColumn] = "@";
          }
        }
        break;
      case "A":
        if (playerColumn - 1 >= 0) {
          if (!(laberinth[playerRow][playerColumn - 1].equals("#"))) {
            counter++;
            laberinth[playerRow][playerColumn] = " ";
            playerColumn--;
            laberinth[playerRow][playerColumn] = "@";
          }
        }
        break;
      case "D":
        if (playerColumn + 1 < laberinth.length) {
          if (!(laberinth[playerRow][playerColumn + 1].equals("#"))) {
            counter++;
            laberinth[playerRow][playerColumn] = " ";
            playerColumn++;
            laberinth[playerRow][playerColumn] = "@";
          }
          break;
        }
      case "P":
        retire = true;
        break;
    }
  }
  /**
   * Aqui el usuario puede crear su propio laberinto
   * @return Devuelve el laberinto creado por el usuario
   */
  public static String[][] cargarTabla() throws IOException, InterruptedException {

    playerRow = -1;
    playerColumn = -1;
    winRow = -1;
    winColumn = -1;
    nivellT = "Creada";
    System.out.println("Porfavor introduzca el numero de filas");
    Scanner scan = new Scanner(System.in);
    files = scan.nextInt();
    System.out.println("Porfavor introduzca el numero de columnas");
    columnes = scan.nextInt(); // Esto deja un ENTER extra...
    laberinth = new String[files][columnes];
    System.out.println("Laberinto de " + files + " x " + columnes);
    System.out.println("Porfavor rellena el laberinto! (Mira el tutorial si tienes alguna duda!!!)");

    // Esta linia es un falso INPUT para evitar que se guarde el  scan de columnas y se pase aqui abajo
    String temp1 = scan.nextLine();
    for (int i = 0; i < files; i++) {
      String input = ""; // Creamos un string sin nada para que entre dentro el WHILE
      while (input.length() != columnes) {
        input = scan.nextLine();
        if (input.length() != columnes) {
          // Si el numero ES diferente a COLUMNAS, imprime el mensaje ERROR, si el input length ES igual a columnas
          // entonces se va al FOR.
          System.out.println("Error, columna no es de " + columnes);
        }
      }
      for (int h = 0; h < input.length(); h++) {
        String temp = Character.toString(input.charAt(h));
        laberinth[i][h] = temp;
        if (temp.equals("!")){
          winRow = i;
          winColumn = h;
        }
        if (temp.equals("@")){
          playerRow = i;
          playerColumn = h;
        }
      }
    }
    boolean win = false;
    boolean retire = false;

    return laberinth;
  }

  /**
   * Esta funcion es la funcion principal de mostrar los datos dentro del save.txt
   */
  public static void mostrarDatos() throws IOException, InterruptedException {

    Scanner scan = new Scanner(System.in);
    System.out.println("Dificultad");
    System.out.println("1. Easy");
    System.out.println("2. Medio");
    System.out.println("3. Hard");
    System.out.println("4. Generados");
    System.out.println("5. Todo");
    //difi y difiAux es lo mismo como en seleccionar(), primero preguntamos el input, miramos si es realmente un nuemro y si es, entonces lo convertimos
    // si no es, entonces le volvemos a preguntar.
    int difi = 0;
    String difiAux;
    difiAux = scan.nextLine();
    if (Character.isDigit(difiAux.charAt(0))){
      difi = Integer.parseInt(difiAux);
    }
    if (difi == 1) nivellT = "Facil";
    else if (difi == 2) nivellT = "Medio";
    else if (difi == 3) nivellT = "Dificil";
    else if (difi == 4)  nivellT ="Generado";
    else if (difi == 5)  nivellT ="";
    else mostrarDatos();
    ensenarDatos(nivellT);
    seleccionar();
  }
  /**
   * Esta funcion es principalmente solo para enseñar dentro del save.txt dependiendo de mostrarDatos
   * @param nivellT es la palabra de la dificultad que dependiendo que numero has seleccionado, sera lo que saldra por pantalla
   */
  public static void ensenarDatos(String nivellT) throws IOException {
    //Primero creamos el BufferedReader para poder leer el archivo save.txt
    BufferedReader in = new BufferedReader(new FileReader("save.txt"));
    String line;
    while ((line = in.readLine()) != null) {
      //En el save.txt en si no interpreta el \n por lo cual si miramos, estara todo en una linia.
      // Pero si lo miramos desde IntelliJ si que vemos el \n en accion.
      // Lo que estamos haciendo aqui es que reeemplazamos la , por un salto de linia.
      // Ejemplo: Dificultad Facil, Movimientos 7
      // Hara un salto de linia entre Facil y Movimiento

      //Como estamos llamando esta funcion para ver, nivellT es el nombre de la dificultad, como por ejemplo Facil, Medio, etc.
      if (line.contains("Nivel: "+nivellT)) {
        line = line.replace(",", "\n");
        if (line.contains("Nivel: "+nivellT)) {
          System.out.println(line);
        }
      }
    }
  }
  /**
   * Esta funcion principalmente sirve para guardar los resultados de los partidos dentro un txt que en este caso es save.txt
   * @param count Numero de movimientos que el jugador ha hecho en la partida
   * @param nivellT dificultad en el que el jugador ha jugado
   */
  public static void guardarInfo (int count, String nivellT) throws InterruptedException, IOException {
    //Primero creamos un PrintWirter y los dirigimos a save.txt y append es true, para que cada vez que escriba no borre lo que ya hay dentro
    PrintWriter out = new PrintWriter(new FileWriter("save.txt",true));

    System.out.println();
    //Hacemos un print de la palabra Nivel: y luego la dificultad que puede ser Easy, Medium etc. Depende de que dificultad el jugador ha jugado
    // nivellT estara elegida cuando el usuario elige el laberinto
    out.print("Nivel: " + nivellT + ",");
    //Cuando pongamos los movimientos al final introducimos un salte de linia. Este salte de linia no se vera mirando directamente desde save.txt
    // Pero en IntelliJ si
    out.print("Movimientos: " + count + "\n");
    out.close();
    //Cerramos el PrintWriter
    System.out.println("Wow! Has logrado el nivel "+ nivellT.toLowerCase());
    TimeUnit.SECONDS.sleep(3);
    System.out.println("En total, has hecho " + count + " movimientos");
    TimeUnit.SECONDS.sleep(3);

    System.out.println("Volviendo al menu principal");
    TimeUnit.SECONDS.sleep(2);
    seleccionar ();
  }
  /**
   * Esta funcion se encarga de mostrar el tutorial de como crear tu propio laberinto y ademas cuando creas la tabla llamara a la funcion  cargarTabla
   *
   */
  public static void crearTabla() throws InterruptedException, IOException {

    Scanner scan = new Scanner(System.in);
    System.out.println("_________________________");
    System.out.println(" 1. Tutorial");
    System.out.println(" 2. Crear Tabla");
    System.out.println("_________________________");
    int opcion= 0;
    String opcionAux;
    opcionAux = scan.nextLine();
    if (Character.isDigit(opcionAux.charAt(0))){
      opcion=Integer.parseInt(opcionAux);
    }


    if (opcion == 1) {
      System.out.println("En esta pantalla, podras cargar tus propios laberintos y jugarlas!");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("Pero tienes que seguir algunas pautas!");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("Las paredes tienen que ser #");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("Los caminos tienen que ser espacios, solo 1 espacio  ' ' ");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("El inicio tiene que estar representado con un @ ");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("El final tiene que estar representado con un ! ");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Tienes que introducir la fila y columan del comienzamiento del jugador");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Tienes que introducir la fila y columna el final del juego");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Ejemplo de cargar laberinto");
      System.out.println("_________________________");
      System.out.println(" 1. Tutorial");
      System.out.println(" 2. Cargar Tabla");
      System.out.println("_________________________");
      opcion = scan.nextInt();
    } else if (opcion == 2){
      nivell = 8; // Estamos manualmente cambiando el nivel para que luego continue con el guardar (Mira la explicacion de printarLaberint)
      printarLaberint(cargarTabla());
    } else {
      crearTabla();
    }
  }
}